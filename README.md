
# World Cup Sweepstake Fun

A little Chrome extension that substitutes team names on the https://www.fifa.com/worldcup microsite with names from a sweepstake.

## Installation

* Clone or download the repo.
* Follow https://developer.chrome.com/extensions/getstarted#unpacked. :)

## Caveats

* It's not big and it's not clever - just does a straight text replacement in the markup so YMMV and you might see sme strange things.