
var teamReplacements = {
  "uruguay": "Duren",
  "russia": "Phil",
  "saudi arabia": "Aiden",
  "egypt": "Zoey",

  "spain": "Lisa",
  "portugal": "Luke",
  "ir iran": "Jaydon",
  "morocco": "Joe",

  "france": "Luke",
  "denmark": "Colin",
  "peru": "Colin",
  "australia": "Chris L",

  "croatia": "Sam",
  "argentina": "Brendan",
  "nigeria": "Andy",
  "iceland": "Jim",

  "brazil": "Jake",
  "switzerland": "Silke",
  "serbia": "David",
  "costa rica": "Payal",

  "mexico": "Chris W",
  "germany": "Thomas",
  "sweden": "Mel Cross",
  "korea republic": "Amy Harding",

  "england": "Nuno",
  "belgium": "Mike",
  "tunisia": "Beckie",
  "panama": "Rachel",

  "japan": "James",
  "senegal": "Elliot",
  "colombia": "Stacey Lalande",
  "poland": "Deano Bambino"
}

var content = document.getElementsByClassName('content-wrap')[0];

var re = new RegExp(Object.keys(teamReplacements).join("|"),"gi");
content.innerHTML = content.innerHTML.replace(re, function(matched){
  return teamReplacements[matched.toLowerCase()];
});
